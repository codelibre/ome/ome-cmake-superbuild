# OME CMake super-build

This package provided a CMake super-build for the
[OME Files C++](http://gitlab.com/codelibre/ome-files)
library, and other additional libraries.  It has been
superseded by Microsoft
[vcpkg](https://github.com/Microsoft/vcpkg).

See [OME Files](https://gitlab.com/codelibre/ome-files) for
the super-build replacement which builds all OME Files
components.

See [OME Files C++](https://gitlab.com/codelibre/ome-files-cpp/)
for links to the current documentation and build instructions.
